'**** Script to populate Synbleez table in SynoviaEnterprizeReporting database *****'
'Input Folderlist : C:\SYNInt\Appfolders.txt
'Input Serverlist : C:\SYNInt\synbleez_srv.txt
'Created : 9/21/2017
'Modified : 02/13/18
'***********************************************
Const ForReading = 1
Const ForWriting = 2
Const adOpenStatic = 3
Const adLockOptimistic = 3

Dim myfile 
Set objFSO = CreateObject("Scripting.FileSystemObject")
Set myfile = objFSO.OpenTextFile("C:\SYNInt\synbleez_srv.txt", 1)
Set objConnection = CreateObject("ADODB.Connection")
Set objRecordSet = CreateObject("ADODB.Recordset")
objConnection.Open "Provider=SQLOLEDB;Data Source=ordutildb;Trusted_Connection=Yes;Initial Catalog=SynoviaEnterpriseReporting;User ID=synoviadbbuilder;Password=synovia;"

objConnection.Execute "TRUNCATE TABLE [SYN_Synbleez]"

Do While Not myfile.AtEndOfStream
	getfileprop myfile.ReadLine   
Loop

Sub getfileprop(websrv)
	''On Error Resume Next
	Set fso = CreateObject("Scripting.FileSystemObject")
	Set objTextFile = objFSO.OpenTextFile("c:\SYNInt\Appfolders.txt", ForReading)
	'Create List 
	Set list = CreateObject("ADOR.Recordset")
	list.Fields.Append "name", 200, 255
	list.Fields.Append "date", 7
	list.Fields.Append "version", 200, 255
	list.Open
	
	Do Until objTextFile.AtEndOfStream
		objpath = objTextFile.ReadLine
		objpath = Right(objpath,Len(objpath)-2)	
		
		If fso.FolderExists("\\" & websrv & "\c$" & objpath) And objpath <> "" Then

			'Populate list
			For Each f In fso.GetFolder("\\" & websrv & "\c$" & objpath).Files
				
				If InStr(f.Name,"Synovia") And Right(f.name,4) = ".dll" Then
					list.AddNew
					list("name").Value = f.Name
					list("date").Value = f.DateLastModified
					list("version").value = fso.GetFileVersion(f.Path)
					list.Update
				End If  
			Next
			
			'Move through list for sort
			list.MoveFirst
			Do Until list.EOF
				'WScript.Echo list("date").Value & vbTab & list("name").Value
				list.MoveNext
			Loop
			
			list.Sort = "date DESC"
			'Get most recent
			list.MoveFirst
			Dim mfile
			Dim mdate
			Dim fvers
			mdate = FormatDateTime(list("date").Value,2)
			mfile = list("name").Value
			fvers = list("version").Value
			
			'WScript.Echo mdate & vbTab & mfile
			
			objRecordSet.Open  "INSERT INTO SYN_Synbleez (Server,Path,Dll,Mod_Date,version) VALUES ('" + websrv + "','" + objpath + "','" + mfile + "','" + mdate + "','" + fvers + "')" , objConnection, adOpenStatic, adLockOptimistic
			'Clear list 
			Do While Not list.EOF
				list.Delete
				list.MoveNext
			Loop
			
			mdate = ""
			mfile = ""
			fvers = ""
			
		End If	
		
	Loop
	
	list.Close
	objTextFile.Close
End Sub



